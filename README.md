# update_decryptor

This docker image can decrypt update files for your Headunit(HU). You must provide the DecryptToPIPE and decryption_key.der files yourself. These files cannot be provided by the developers and we heavily discourage you from sharing these files.

## How to build the image?
1. Clone this repository
1. Go to the folder where you cloned this project and run:
```bash
docker build -t gen5wdecryptor ./
```

## How to decrypt an update?
1. Download an official update for your Headunit. Go over to the [update_fetcher](https://gitlab.com/g4933/gen5w/update_fetcher) project to see how.
1. Go to the folder where you downloaded your update and run: 
```bash
docker run --rm -it -v $PWD:/mnt gen5wdecryptor
```
- It will create a folder named `decrypted` and it will contain the decrypted files. Only files that were actually encrytped will end up in the `decrypted` folder. If no files were decrypted then no folder will be created.

## How does it look?

- ![decrypt process](images/decrypt_process.jpg)
